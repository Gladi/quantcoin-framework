use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::{TcpListener, TcpStream};
use std::net::SocketAddr;
use std::thread; // std::net::SocketAddr not work!

pub enum IpMode {
    Ipv6Only,
    Ipv4Only
}

pub struct Server {
    ip_mode: IpMode,
    port: i32
}

impl Server {
    async fn new_connection(&self, tcp_stream: TcpStream, socket_addr: SocketAddr) {

    }

    fn convert_ip_mode(&self) -> String {
        match self.ip_mode {
            IpMode::Ipv4Only => String::from(format!("127.0.0.1:{}", self.port)),
            IpMode::Ipv6Only => String::from(format!("[::]:{}", self.port))
        }
    }

    pub async fn run(&self) {
        // Тут должна быть инцидлизации среды для tokio
        // Она в макросе tokio:main
        let listener = TcpListener::bind( self.convert_ip_mode()).await.unwrap();

        loop {
            let (tcp_stream, socket_addr) = listener.accept().await.unwrap();

            thread::spawn() // tokio::spawn
            self.new_connection(tcp_stream, socket_addr);
        }
    }
}